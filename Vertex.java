package loicp;

/**
 * Created by paulettl on 13/01/16.
 */
public class Vertex {
    final private String id;

    public Vertex(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return id;
    }
}