package loicp;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by paulettl on 13/01/16.
 */
public class Main {


    private static Graph graph;

    public static int[][] convertInMatrix(){

        int matrix[][] = null;

        try {
            String line;
            // FileReader reads text files in the default encoding.
            FileReader fileReader = new FileReader("PPC.dat");

            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            line = bufferedReader.readLine();
            int nbVertex = Integer.parseInt(line.trim());

            matrix = new int[nbVertex][nbVertex];

            for (int i = 0; i < matrix.length; ++i){
                String[] cur_line = bufferedReader.readLine().split("\\s+");
                for (int j = 0; j < matrix[i].length; ++j){
                    matrix[i][j] = Integer.parseInt(cur_line[j]);
                }
            }

            // Always close files.
            bufferedReader.close();

        }
        catch(FileNotFoundException ex) {
            System.out.println(
                    "Unable to open file PCC.dat ");
            System.exit(0);
        }
        catch(IOException ex) {
            System.out.println(
                    "Error reading file PCC.dat ");
            // Or we could just do this:
            // ex.printStackTrace();
        }

        return matrix;
    }

    public static Graph initGraph(int[][] matrix){

        List<Vertex> nodes = new ArrayList<Vertex>();
        List<Edge> edges = new ArrayList<Edge>();
        int lenght = matrix.length;

        for (int i = 0; i < matrix.length; i++) {
            nodes.add(new Vertex("Vertex_"+i));
        }


        int number = 0;

        for (int i = 0; i < matrix.length; i++){
            for (int j = 0 ; j < matrix.length; j++){
                if(matrix[i][j] != 999){
                    edges.add(new Edge("Edge_"+number,nodes.get(i),nodes.get(j),matrix[i][j]));
                    number++;
                }
            }
        }

        return new Graph(nodes,edges);
    }

    public static void printPathAndDistance(Djikstra djikstra){
        for (int i = 1; i < 10; ++i) {

            LinkedList<Vertex> path = djikstra.getPath(graph.getVertexes().get(i));

            Integer distance = djikstra.getDistance().get(graph.getVertexes().get(i));
            System.out.println("Distance : " + distance);
            System.out.println("Points : ");
            for (Vertex vertex : path) {
                System.out.println('\t' + vertex.toString());
            }
            System.out.println();
        }

    }

    public static void main(String[] args) {

        int[][] matrix;
        matrix = convertInMatrix();

        graph = initGraph(matrix);

        Djikstra djikstra = new Djikstra(graph);

        djikstra.execute(graph.getVertexes().get(0));
        printPathAndDistance(djikstra);


    }
}
